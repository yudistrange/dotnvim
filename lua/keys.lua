-- [[ keys.lua ]]

local map = vim.api.nvim_set_keymap

-- remap the key used to leave insert mode
map('i', 'jj', '<Esc>', { noremap = true })

-- C-g to escape
map('', '<C-g>', '<Esc>', { noremap = true })
map('n', '<C-g>', '<Esc>', { noremap = true })
map('i', '<C-g>', '<Esc>', { noremap = true })
map('v', '<C-g>', '<Esc>', { noremap = true })
map('c', '<C-g>', '<Esc>', { noremap = true })

-- <Space>w to save
map('', '<leader>w', '<C-c>:update<cr>', { noremap = true })
map('n', '<leader>w', '<C-c>:update<cr>', { noremap = true })
map('v', '<leader>w', '<C-c>:update<cr>', { noremap = true })

-- <Space>q to quit
map('', '<leader>q', '<C-c>:q<cr>', { noremap = true })
map('n', '<leader>q', '<C-c>:q<cr>', { noremap = true })
map('v', '<leader>q', '<C-c>:q<cr>', { noremap = true })

-- Toggle nvim-tree
map('n', '<leader>nn', ':NvimTreeToggle<CR>', { noremap = true })
map('n', '<leader>tt', ':TagbarToggle<CR>', { noremap = true })

-- Telecope shortcuts
map('n', '<leader>ff', ':Telescope find_files<CR>', { noremap = true })
map('n', '<leader>fg', ':Telescope git_files<CR>', { noremap = true })
map('n', '<leader>fb', ':Telescope buffers<CR>', { noremap = true })
map('n', '<leader>fr', ':Telescope live_grep<CR>', { noremap = true })
map('n', '<leader>fo', ':Telescope oldfiles<CR>', { noremap = true })

-- Toggle floating terminal
map('n', '<leader>=', ':FloatermToggle<CR>', { noremap = true })
map('t', '<leader>=', [[<C-\><C-n>:FloatermToggle<CR>]], { noremap = true })

-- Split window
map('n', '<leader>sh', ':sp<CR>', { noremap = true })
map('n', '<leader>sv', ':vsp<CR>', { noremap = true })

-- Tabs
map('n', '<C-t>', ':tabnew<CR>', { noremap = true })
map('n', '<C-tab>', ':tabnext<CR>', { noremap = true })
map('n', '<C-S-tab>', ':tabprevious<CR>', { noremap = true })

-- Undo tree toggle
map('n', '<C-u', ':UndotreeToggle<CR>', { noremap = true })
map('n', '<C-u>', ':UndotreeToggle<CR>', { noremap = true })

-- Allow clipboard copy paste in neovim
vim.g.neovide_input_use_logo = 1
map('', '<C-v>', '+p<CR>', { noremap = true, silent = true })
map('!', '<C-v>', '<C-R>+', { noremap = true, silent = true })
map('t', '<C-v>', '<C-R>+', { noremap = true, silent = true })
map('v', '<C-v>', '<C-R>+', { noremap = true, silent = true })
