-- Add additional capabilities supported by nvim-cmp
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

local lspconfig = require('lspconfig')
-- Mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local opts = { noremap = true, silent = true }
vim.keymap.set('n', 'ldd', vim.diagnostic.open_float, opts)
vim.keymap.set('n', 'ldn', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', 'ldp', vim.diagnostic.goto_next, opts)

-- Enable auto installer
require("nvim-lsp-installer").setup({
  automatic_installation = true, -- automatically detect which servers to install (based on which servers are set up via lspconfig)
  ui = {
    icons = {
      server_installed = "✓",
      server_pending = "➜",
      server_uninstalled = "✗"
    }
  }
})

-- Format using lsp
require("lsp-format").setup {}

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Enable formatting
  require("lsp-format").on_attach(client)

  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap = true, silent = true, buffer = bufnr }
  vim.keymap.set('n', 'lgD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'lgd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'lh', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'lgi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', 'lsh', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', 'lwa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', 'lwr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', 'lwl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', 'ltd', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', 'lrn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', 'lca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'lgr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', 'lf', function() vim.lsp.buf.format { async = true } end, bufopts)
end

local lsp_flags = {
  -- This is the default in Nvim 0.7+
  debounce_text_changes = 150,
}

-- Enable some language servers with the additional completion capabilities offered by nvim-cmp
local servers = { 'gopls', 'clojure_lsp', 'yamlls', 'sumneko_lua', 'elixirls' }
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    flags = lsp_flags,
    capabilities = capabilities,
    settings = {
      Lua = {
        diagnostics = {
          globals = { 'vim' }
        }
      }
    }
  }
end
