require('hop').setup({
  multi_windows = true,
  jump_on_sole_occurrence = true,
})

vim.api.nvim_set_keymap('n', '<C-s>', '<C-c>:HopPattern<CR>', {})
