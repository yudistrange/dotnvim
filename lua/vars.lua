--[[ vars.lua ]]

vim.g.t_co = 256            -- Set the term colors to 256
vim.g.background = "dark"   -- Set the background to dark

-- Update the packpath
local packer_path = vim.fn.stdpath('config') .. '/site'
vim.o.packpath = vim.o.packpath .. ',' .. packer_path
