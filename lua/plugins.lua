-- [[ plugin.lua ]]

return require('packer').startup(function(use)
  -- [[ use {ins Go Here ]]

  use { 'wbthomason/packer.nvim' } -- self manage Packer
  use { -- nvim-tree
    'nvim-tree/nvim-tree.lua',
    requires = {
      'nvim-tree/nvim-web-devicons',
    }
  }

  -- [[ UI/UX ]]
  use { 'mhinz/vim-startify' } -- start screen
  use { 'DanilaMihailov/beacon.nvim' } -- cursor jump

  use {
    'glepnir/galaxyline.nvim', -- status-line
    branch = 'main',
    requires = { 'kyazdani42/nvim-web-devicons' }
  }
  use { 'Mofiqul/dracula.nvim' } -- colorscheme
  use {
    'catppuccin/nvim',
    as = 'catppuccin',
    config = function()
      vim.g.catppuccin_flavour = 'macchiato' -- latte, frappe, macchiato, mocha
      require('catppuccin').setup()
    end
  }

  -- [[ Dev ]]
  use {
    'nvim-telescope/telescope.nvim', -- fuzzy finder
    requires = { { 'nvim-lua/plenary.nvim' } }
  }
  use { 'majutsushi/tagbar' } -- code structure
  use { 'Yggdroot/indentLine' } -- see indentation
  use { 'tpope/vim-fugitive' } -- git integration
  use { 'junegunn/gv.vim' } -- commit history
  use { 'windwp/nvim-autopairs' } -- auto close bracket
  use { 'voldikss/vim-floaterm' } -- floating terminal
  use { 'nvim-treesitter/nvim-treesitter' }
  use { 'guns/vim-sexp' }
  -- use { 'ggandor/leap.nvim' }
  use { 'phaazon/hop.nvim' }
  use { 'p00f/nvim-ts-rainbow' }
  use { 'mfussenegger/nvim-lint' }
  use { 'folke/which-key.nvim' }
  use { 'kylechui/nvim-surround' }
  use { 'lukas-reineke/lsp-format.nvim' }

  -- [[ LSP Setting ]]
  use { 'neovim/nvim-lspconfig' } -- LSP Config
  use { 'williamboman/nvim-lsp-installer' }
  use { 'hrsh7th/nvim-cmp' } -- Autocompletion plugin
  use { 'hrsh7th/cmp-nvim-lsp' } -- LSP source for nvim-cmp

  -- [[ Language specific plugins ]]
  use { 'Olical/conjure' } -- clojure plugin
  use { 'clojure-vim/vim-jack-in' }
  use { 'tpope/vim-dispatch' }
  use { 'radenling/vim-dispatch-neovim' }
  use { 'PaterJason/cmp-conjure' }
  use {
    'folke/trouble.nvim',
    requires = 'kyazdani42/nvim-web-devicons',
    config = function()
      require('trouble').setup {}
    end
  }
  use {
    'numToStr/Comment.nvim',
  }
  use { 'creativenull/efmls-configs-nvim' }
  use { 'yamatsum/nvim-cursorline' }
  use { 'j-hui/fidget.nvim' }
  use { 'mhanberg/elixir.nvim' }
  use { 'mbbill/undotree' }
end)
