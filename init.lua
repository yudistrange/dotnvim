-- [[ init.lua ]]

-- LEADER
vim.g.mapleader = " " -- Set leader key to space
vim.g.maplocalleader = "," -- & localleader to ,

-- IMPORTS
require('vars') -- Variables
require('opts') -- Options
require('keys') -- Keymaps
require('plugins') -- Plugins

-- Plugin configuration
require('nvim-tree').setup { update_cwd = true }
require('nvim-autopairs').setup {}
require('plugins/galaxyline-evil')
require('nvim-autopairs').setup {}

-- require('leap').add_default_mappings()
require('plugins/lint-config')
require('plugins/cmp-config')
require('Comment').setup()
require('which-key').setup()
require('nvim-surround').setup()
require('plugins/cursorline')
require('plugins/hop-config')
require('fidget').setup {}
require('elixir').setup {}
require('plugins/ts-config')
require('plugins/lsp-config')
-- Used the very helpful guide present here
-- https://mattermost.com/blog/how-to-install-and-set-up-neovim-for-code-editing/
